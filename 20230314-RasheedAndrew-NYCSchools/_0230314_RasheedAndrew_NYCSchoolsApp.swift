//
//  _0230314_RasheedAndrew_NYCSchoolsApp.swift
//  20230314-RasheedAndrew-NYCSchools
//
//  Created by rasheed andrew on 3/14/23.
//

import SwiftUI

@main
struct _0230314_RasheedAndrew_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
